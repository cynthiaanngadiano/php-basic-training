<?php
include ("connection.php");
require_once ("validation_functions.php");

session_start();
if(isset($_SESSION["username"]))
{
    header("location: welcome.php");
}

if (isset($_COOKIE['username']) && isset($_COOKIE['password'])) {
    $uname = $_COOKIE['username'];
    $passw = $_COOKIE['password'];
    $query = mysqli_query($connection, "SELECT * FROM users WHERE username = '$uname' AND password = '$passw'");$rows = mysqli_num_rows($query);
    if($rows == 1){
        $_SESSION["username"] = $uname;
        header("location: welcome.php");
    }
    mysqli_close($connection); // Closing connection
}

$errors = array();
$msg = "";

if (isset($_POST['login'])) {
        if (!empty($_POST["username"]) && !empty($_POST["password"])){
            if (empty($errors)) {
                //Define $user and $pass
                $uname = mysqli_real_escape_string($connection,$_POST["username"]);
                $passw = mysqli_real_escape_string($connection,$_POST["password"]);
                //prevent sql injection
                $uname = stripcslashes($uname);
                $passw = stripcslashes($passw);
                //sql query to fetch information of registerd user and finds user match.
                $query = mysqli_query($connection, "SELECT * FROM users WHERE username = '$uname' AND password = '$passw'");
                $rows = mysqli_num_rows($query);
                if($rows == 1){
                    if(!empty($_POST["remember"]))
                    {
                        setcookie ("username",$uname,time() + (60*60*24*7));
                        setcookie ("password",$passw,time() + (60*60*24*7));

                    }
                    $_SESSION["username"] = $uname;
                    header("location: welcome.php"); // Redirecting to welcome page
                } else {
                    $msg = "Incorrect Username or Password";
                }
                mysqli_close($connection); // Closing connection
            }
        }

    $uname = trim($_POST['username']);
    $pw= trim($_POST['password']); //submit form

    $fields_required = ["username","password"];
    foreach ($fields_required as $field) {
        $value = trim($_POST[$field]);
        if (!presence($value)) {
            $errors[$field] = ucfirst($field) . " cannot be blank.";
        } //check if uname is empty
    }

    $fields_max_length =[
        "username" => 20,
        "password" => 10
    ];
    validate_max_lengths($fields_max_length);
}
?>
<html>

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>Login Page</title>

</head>
<body>
<?php echo $msg; ?>
<?php echo form_errors($errors); ?>
<div align = "center" style="padding-top: 150px">
    <div style = "width:300px" align = "left">
        <div class="panel panel-default">
            <div class="panel-heading">LOGIN</div>
            <div class="panel-body">
                <form action = "<?php $_SERVER['PHP_SELF']; ?>" method = "post">
                    <div class="form-group">
                        <label for="username">Name:</label>
                        <input type="text" class="form-control" name="username" value="<?php if(isset($_COOKIE["username"])) { echo $_COOKIE["username"]; } ?>" autofocus>
                    </div>
                    <div class="form-group">
                        <label for="password">Password:</label>
                        <input type="password" class="form-control" name="password" value="<?php if(isset($_COOKIE["password"])) { echo $_COOKIE["password"]; } ?>">
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="remember" <?php if(isset($_COOKIE["username"])) { ?> checked <?php } ?>>
                        <label for="remember">Remember me</label>
                    </div>
                    <button type="submit" name="login" class="btn">Login</button>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<?php
    session_start();
    unset($_SESSION["username"]);
    setcookie('username', null, -1);
    setcookie('password', null, -1);
    header("location:log-in.php");
<?php
session_start();
if(!isset($_SESSION["username"]))
{
    header("location: log-in.php");
}
?>
<html>
<head>
    <title>Welcome</title>
    <style>
        .container h1{
            border: 1px solid red;
            border-radius: 5px;
            padding: 30px;
            margin: 0 auto;
            position: absolute;
            top: 50%;
            left: 50%;
            margin-top: -50px;
            margin-left: -105px;
            font-family: Trebuchet MS;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>WELCOME</h1>
        <p><a href="log-out.php">Logout</a></p>
    </div>
</body>
</html>
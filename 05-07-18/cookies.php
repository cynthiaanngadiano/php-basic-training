    <?php
        $name = "test";
        $value = "Geo"; //
        $expire = time() /*-UNIX Time Stamp*/ + (60*60*24*7); //expiry time in seconds
        //setcookie($name, $value, $expire);
                //wrong way to unset a cookie, unset()
        //setcookie($name); //unset the cookie
        //setcookie($name, null, $expire); unset the cookie
        //setcookie($name, $value, time() - 3600); unset the cookie
        //setcookie($name, null,time() - 3600); unset the cookie MOST RECOMMENDED
    ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cookies</title>
</head>
<body>
    <?php
        $test = isset($_COOKIE["test"]) ? $_COOKIE["test"] : "";
        echo $test;
    ?>
</body>
</html>
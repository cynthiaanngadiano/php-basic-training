<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Validations</title>
</head>
<body>
    <?php
        // PRESENCE
        $value = "hehe";
        if (!isset($value) || empty($value)) {
            echo "The value: {$value}, is not set or empty<br />";
        } else {
            echo "The value: {$value}, is set or empty<br />";
        }
        //STRING LENGTH
        //minimum length
        $value = "5";
        $min = 3;
        if (strlen($value) < $min) {
            echo "The length of the string: {$value}, is lesser than {$min}<br />";
        } else {
            echo "The length of the string: {$value}, is not lesser than {$min}";
        }
        $value = "10";
        $max = 6;
        if (strlen($value) > $max) {
            echo "The length of the string: {$value}, is greater than {$max}<br />";
        } else {
            echo "The length of the string: {$value}, is not greater than {$max}<br />";
        }
        //TYPE
        $value = "yeyeye";
        if (!is_string($value)) {
            echo "The value: {$value}, is not a string<br />";
        } else {
            echo "The value: {$value}, is a string<br />";
        }
        //INCLUSION IN A SET
        $value = "5";
        $set = ["1","2","3","4"];
        if (!in_array($value, $set)) {
            echo "The value: {$value}, is not included in the set: ";
            echo "<pre>";
            print_r(array_values($set));
            echo "<br />";
            echo "</pre>";
        } else {
            echo "The value: {$value}, is included in the set: ";
            echo "<pre>";
            print_r(array_values($set));
            echo "<br />";
            echo "</pre>";
        }

        //UNIQUENESS, uses database to check uniqueness

        //FORMAT, use a regex on a string, preg_match($regex, $subject)
        $subject = "PHP is fun";
        $regex = "/PHP/";
        if (preg_match($regex, $subject)) { // or ("/PHP/", "PHP is fun")
            echo "{$regex} was found in {$subject}";
        } else {
            echo "{$regex} was not found in {$subject}";
        }
    ?><br />
</body>
</html>
    <?php
    function redirect ($new_location) {
        header("Location: " . $new_location);
        exit;
    }
    $logged_in = $_GET['logged_in'];
    if ($logged_in == '1') {
        redirect("headers.php");
    } else {
        redirect("includes.php");
    }
    ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Redirect</title>
</head>
<body>
</body>
</html>
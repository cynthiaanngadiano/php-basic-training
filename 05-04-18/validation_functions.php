<?php
// PRESENCE
function presence($value) {
    return isset($value) && $value !== "";
}

//STRING LENGTH
//maximum length
function maxLength($value, $max){
    return strlen($value) < $max;
}

//INCLUSION IN A SET
function inclusion($value, $set) {
    return in_array($value, $set);
}

function validate_max_lengths($fields_max_length) {
    global $errors;
    foreach ($fields_max_length as $field => $max) {
        $value = trim($_POST[$field]);
        if (!maxLength($value, $max)) {
            $errors[$field] = ucfirst($field) . " is too long";
        }
    }
}

function form_errors($errors=array()) {
    $output="";
    if (!empty($errors)) {
        $output .= "<div class=\"error\">";
        $output .= "Please fix the following errors: ";
        $output .= "<ul>";
        foreach ($errors as $key => $error) {
            $output .= "<li>{$error}</li>";
        }
        $output .= "</ul>";
        $output .= "</div>";
    }
    return $output;
}
?>
<?php
    require_once ("included_functions.php");
    require_once ("validation_functions.php");

    $errors = array();
    $msg = "";

    if (isset($_POST['submit'])) {
        $uname = trim($_POST['username']);
        $pw= trim($_POST['password']); //submit form

        $fields_required = ["username","password"];
        foreach ($fields_required as $field) {
            $value = trim($_POST[$field]);
            if (!presence($value)) {
                $errors[$field] = ucfirst($field) . " cannot be blank.";
            } //check if uname is empty
        }

        $fields_max_length =[
            "username" => 20,
            "password" => 10
        ];
        validate_max_lengths($fields_max_length);

        if (empty($errors)) {
            //try to log in
            if ($uname=="cyn" && $pw=="secret") {
                redirect("../05-02-18/my_phpinfo.php"); //sucessful login
            } else {
                $msg = "Incorrect username or password";
            }
        }

    } else {
        $uname = "";
        $msg = "Please log in.";
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Form with validation</title>
</head>
<body>
    <?php echo $msg; ?><br />
    <?php echo form_errors($errors); ?><br />
    <form action="form_with_validation.php" method="POST">
        Username: <input type="text" name="username" value="<?php echo htmlspecialchars($uname); ?>" autofocus /><br />
        Password: <input type="password" name="password" /><br /><br />
        <input type="submit" name="submit" value="SUBMIT" />
    </form>
</body>
</html>
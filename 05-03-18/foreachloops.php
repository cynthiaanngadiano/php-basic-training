<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Loops: For Each</title>
</head>
<body>
    <?php
        $ages = [4,8,15,16,23,42];

        foreach ($ages as $age) {
            echo "Age: {$age}<br />";
        }
    ?><br />
    <?php
        $person = [
            "first_name" => "Cynthia Ann",
            "last_name" => "Gadiano",
            "address" => "Candon City, Ilocos Sur",
            "zipcode" => "2710",
        ];

        foreach ($person as $attribute => $data) {
            $attr_nice = ucwords(str_replace("_", " ", $attribute));
            echo "{$attr_nice}: {$data} <br />";
        }
    ?>
    <?php
        $prices = [
            "Brand New Computer" => 2000,
            "1 month of Lynda.com" => 25,
            "Learning PHP" => null,
        ];

        foreach ($prices as $item=> $price) {
            echo "{$item} : ";
            if (is_int($price)) {
                echo "Php " . $price;
            }
            else {
                echo "Priceless";
            }
            echo "<br />";
        }
    ?>
</body>
</html>
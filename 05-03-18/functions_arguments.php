<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Functions: Arguments</title>
</head>
<body>
    <?php
        function say_hello_to($word) {
            echo "hello {$word}<br />";
        }
        $name = "Cynthia Ann";
        say_hello_to($name);

        //function str_replace($find, $replace, $target)
    ?>
    <?php
        function better_hello ($greeting, $target, $punct) {
            echo $greeting . " " . $target . $punct . "<br />";
        }

        better_hello("Hello", $name, "!!!!");
        better_hello("Hello", $name, null);
    ?>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>First Page</title>
</head>
<body>
    <?php
        $page = "William Shakespeare";
        $quote = "To be or not to be";
        $link1 = "/bio/" . rawurlencode($page);
        echo "<b>rawurlencode : </b>" . $link1 . "<br />";
        $link2 = "/bio/" . urlencode($quote);
        echo "<b>urlencode : </b>" . $link2 . "<br />";
    ?>
</body>
</html>
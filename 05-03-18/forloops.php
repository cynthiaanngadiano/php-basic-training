<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Loops: For</title>
</head>
<body>
    <?php //while loop example
    $count = 0;
    while ($count <= 10) {
        echo $count . ", ";
        $count++;
    }
    ?>
    <?php //for loop example
    for ($count=0; $count <= 10; $count++){
        echo $count . ", ";
    }
    ?>
    <br />
    <?php
        for ($num = 1; $num < 20; $num++){ //or ($num = 20; $num < 0; $num--)
            if ($num % 2 == 0){
                echo "The number {$num} is even";
            }
            else {
                echo "The number {$num} is odd";
            }
            echo  "<br />";
        }
    ?>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>HTML encoding</title>
</head>
<body>
    <?php
        $linktext = "<Click> & learn more</Click>";
    ?>
    <a href="">
        <?php echo htmlspecialchars($linktext); //to show the special characters?>
    </a><br />
    <?php
        $text = "☻♦";
        echo htmlentities($text); //para magpakita ang special characters
    ?><br />
    <?php
        $url_page = "php/created/page/url.php";
        $param1 = "This is a string with < >";
        $param2 = "&#?*$[]+ are bad characters";
        $linktext = "<Click> & learn more</Click>";

        $url = "http://localhost/";
        $url .=rawurlencode($url_page);
        $url .= "?" . "param1 = " . urlencode($param1);
        $url .= "&" . "param2 = " . urlencode($param2);
    ?>
    <a href="<?php echo htmlspecialchars($url); ?>">
        <?php echo htmlspecialchars($linktext); ?>
    </a>
</body>
</html>
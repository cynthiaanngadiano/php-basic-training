<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Functions: Defining</title>
</head>
<body>
    <?php
        function say_hello() {
            echo "hello world<br />";
        }
        say_hello();

    function say_hello_to($word) {
        echo "hello {$word}<br />";
    }
    say_hello_to("World");
    say_hello_to("everyone");

    say_hello_loudly();

    function say_hello_loudly() {
        echo "HELLO WORLD<br />";
    }
    ?>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Pointers</title>
</head>
<body>
    <?php
        $ages = [4,8,15,16,23,42];
        //current: current pointer value
    echo "1 : " . current($ages) . "<br />";
    next($ages);
    echo "2 : " . current($ages) . "<br />";
    next($ages);
    echo "3 : " . current($ages) . "<br />";
    next($ages); //can be used twice b4 printing
    echo "4 : " . current($ages) . "<br />";
    prev($ages); //move pointer backwards
    echo "3 : " . current($ages) . "<br />";
    reset($ages); //move pointer to the first element
    echo "1 : " . current($ages) . "<br />";
    end($ages); //move pointer to the last element
    echo "6 : " . current($ages) . "<br />";
    ?><br />
    <?php
    reset($ages);
        while($age = current($ages)) {
            echo $age . ", ";
            next($ages);
        }
    ?>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Debugging</title>
</head>
<body>
    <?php
        $number = 99;
        $string = "BUG?";
        $array = [
            1 => "Homepage",
            2 => "ABout Us",
            3 => "Services",
        ];

        var_dump($number);
        var_dump($string);
        var_dump($array);
    ?><br />
    <pre>
        <?php
          //  print_r(get_defined_vars());
        ?>
    </pre><br />
    <?php
        var_dump(debug_backtrace());
    ?>
</body>
</html>
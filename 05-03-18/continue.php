<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Continue</title>
</head>
<body>
    <?php //for loop example
        for ($count=0; $count <= 10; $count++){
            if ($count % 2 == 0) {
                continue; //loop earlier, loop starts again.
            }
            echo $count . ", ";
        }
    ?><br />
    <?php
        $count = 0;
        while ($count <= 10) {
            if ($count == 5){
                $count++;
                continue; //loop is stucked here if there is no $count++
            }
            echo $count . ", ";
            $count++;
        }
        echo  "<br />";
    ?><br />
    <?php
        for ($i=0; $i<=5; $i++) {
            if ($i % 2 == 0) {
                continue(1);
            }
            for ($k = 0; $k <= 5; $k++) {
                if ($k == 3){continue(2);}
                echo $i . "-" . $k . "<br />";
            }
        }
    ?>
</body>
</html>
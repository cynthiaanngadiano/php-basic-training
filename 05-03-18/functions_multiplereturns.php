<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Functions: Multiple Returns</title>
</head>
<body>
    <?php
        function add_subt($val1, $val2) {
            $add = $val1 + $val2;
            $subt = $val1 - $val2;
            return [$add, $subt]; //to return 2 declared values, make it an array.
        }

        $result = add_subt(10,5);
        echo "Add : " . $result[0] . "<br />";
        echo "Subtract : " . $result[1] . "<br />";

        list($add_result, $subt_result) = add_subt(70,5);
        echo "Add : " . $add_result . "<br />";
        echo "Subtract : " . $subt_result . "<br />";
    ?><br />
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Booleans</title>
</head>
<body>
    <?php
        $var1 = null;
        $var2 = "";
    ?>
    is var1 null? <?php echo is_null($var1); ?><br />
    is var2 null? <?php echo is_null($var2); ?><br />
    is var3 null? <?php echo is_null($var3); //var3 is null bc it is not declared therefore has no value?><br />
    <br />
    is var1 set? <?php echo isset($var1); ?><br />
    is var2 set? <?php echo isset($var2); //var2 is set bc it is set LOL?><br />
    is var3 set? <?php echo isset($var3); ?><br />

    <?php //empty is "", null, 0, 0.0, "0", false, array() or [] ?>
    <?php $var3 = 0; ?><br />
    is var1 empty? <?php echo empty($var1); ?><br />
    is var2 empty? <?php echo empty($var2); ?><br />
    is var3 empty? <?php echo empty($var3); ?><br />
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hello world</title>
</head>
<body>
    <?php
    // single line comments
    # or this one, but this isnt commonly used
    /**comments
            comments**/
    ?>
    <?php echo "Hello world!"; ?> <br />
    <?php echo "Hello" . "world!"; ?> <br />
    <?php echo 2+3; ?> <br />
</body>
</html>

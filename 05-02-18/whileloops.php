<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Loops: While</title>
</head>
<body>
    <?php
        $count = 0;
        while ($count <= 10) {
            if ($count == 5){
                echo "FIVE, ";
            }
            else {
                echo $count . ", ";
            }
            $count++;
        }
        echo  "<br />";
        echo "Count: {$count}";
    ?><br /><br />
    <?php
    $num = 1;
    while ($num <= 20) {
        if ($num % 2){
            echo "The number {$num} is odd";
        }
        else {
            echo "The number {$num} is even";
        }
        $num++;
        echo  "<br />";
    }
    echo  "<br />";
    ?>

</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Floating Point Numbers</title>
</head>
<body>
    <?php
        echo $float = 3.14;
        echo "<br />";
        echo $float + 7;
        echo "<br />";
        echo 4/3;
    ?>
    <br /><br />
    Round:      <?php echo round($float, 1); ?><br />
    Ceiling:    <?php echo ceil($float); ?><br />
    Floor:      <?php echo floor($float); ?><br />
    <br /><br />
    <?php $integer = 3;
        echo "Is {$integer} an integer?" . is_int($integer);
        echo"<br />";
        echo "Is {$float} an integer?" . is_int($float);
        echo"<br /><br />";
        echo "Is {$integer} a float?" . is_float($integer);
        echo"<br />";
        echo "Is {$float} a float?" . is_float($float);
        echo"<br /><br />";
        echo "Is {$integer} a numeric?" . is_numeric($integer);
        echo"<br />";
        echo "Is {$float} a numeric?" . is_numeric($float);
    ?>
</body>
</html>
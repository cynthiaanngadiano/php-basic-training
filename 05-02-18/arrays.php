<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Floating Point Numbers</title>
</head>
<body>
    <?php
        $numbers = array(4,8,15,16,23,42);
        echo $numbers[1];
    ?><br />
    <?php $mixed = array(6, "foxed", "dog", array("x","y","z")); ?>
    <?php echo $mixed[2]; ?><br />
    <?php // echo $mixed[3]; doesn't work ?><br />
    <?php //echo $mixed; doesn't work ?><br />
    <?php echo $mixed[3][1]; ?><br />
    <?php $mixed[2] = "cat"; //change content of index 2?><br />
    <?php $mixed[4] = "mouse"; //to add new index?><br />
    <?php $mixed[] = "horse"; //will add it in the end whatever the end is?><br />
    <pre>
        <?php echo print_r($mixed); ?>
    </pre>

    <?php
        $anArray = [1,2,3]; //easier way to declare and use arrays, the short array syntax for >=5.4.
    ?>
</body>
</html>
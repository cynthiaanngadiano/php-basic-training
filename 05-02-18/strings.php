<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Strings</title>
</head>
<body>
    <?php
        echo "Hello world<br />";
        echo 'Hello world<br />';

        $greeting = "Hello";
        $target = "world";
        $phrase = $greeting . " " . $target;
        echo $phrase;
    ?>
    <br />
    <?php
        echo "$phrase again<br />";
        echo '$phrase again<br />';
        echo "{$phrase}again<br />";
    ?>
</body>
</html>

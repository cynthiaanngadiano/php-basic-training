<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Associative Arrays</title>
</head>
<body>
    <?php
        $assoc = [
            "first_name" => "Zeinth",
            "last_name" => "Reyes"
        ];
        echo $assoc["first_name"]; echo "<br />";
        echo $assoc["first_name"] . " " . $assoc["last_name"]; echo "<br />";
        // echo $numbers[0]; would not work because there is no index 0
        echo "<br />";
        $numbers = [4,8,15,16,23,42];
        $numbers = [0 => 4,1 => 8,2 => 15,3 => 16,4 => 23,5 => 42]; //we can change the index of the contents of array $numbers
        echo $numbers[0];
    ?>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Constants</title>
</head>
<body>
    <?php
        $a = 3;
        $b = 4;

        if ($a > $b){
            echo "a is larger than b";
        }
        elseif ( $a < $b) {
            echo "a is not larger than b";
        }
        else {
            echo "a is equal to  b";
        }
    ?> <br />
    <?php
        $new_user = true;
        if ($new_user) {
            echo "<h1>Welcome!</h1>";
            echo "<p>We are glad you decided to join us.</p>";
        }
    ?>
    <?php
    $numerator= 20;
    $denominator= 4;
    $result= 0;
    if ($denominator > 0) {
        $result = $numerator / $denominator;
    }
    echo "Result: {$result}";
    ?>
</body>
</html>
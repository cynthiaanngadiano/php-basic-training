<?php
require_once("../includes/session.php");
require("../includes/connection.php");
require_once("../includes/functions.php");
$admin_set = find_all_admins();
$layout_context = "admin";
include("../includes/layouts/header.php");?>
<div id="main">
    <div id="navigation">
        <br />
        <a href="admin.php">&laquo; Main Menu</a><br />
    </div>

    <div id="page">
        <?php
            if(isset($_SESSION["created_subject_tracker"]) && $_SESSION["created_subject_tracker"]){
                echo message();
                $_SESSION["created_subject_tracker"] = false;
             }
        ?>
        <h2>Manage Admins</h2>
        
        <table>
            <th style="text-align:left; width:200px;">Username</th>
            <th colspan="2" style="text-align:left;">Actions</th>
            
            <?php while($admin = mysqli_fetch_assoc($admin_set)){?>
                <tr>
                    <td><?php echo htmlentities($admin["username"]);?></td>
                    <td><a href="edit_admin.php?id=<?php echo urlencode($admin["id"]);?>">Edit</a></td>
                    <td><a href="delete_admin.php?id=<?php echo urlencode($admin["id"]);?>" onclick="return confirm('Are you sure you want to DELETE this Admin: <?php echo $admin["username"];?>?');">Delete</a></td>
                </tr>
            <?php }?>
        </table>
        <br />
        <a href="new_admin.php">Add new Admin</a>
    </div>
</div>
<?php include("../includes/layouts/footer.php");?>
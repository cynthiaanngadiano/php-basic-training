<?php
require_once("../includes/session.php");
require("../includes/connection.php");
require_once("../includes/functions.php");
require_once("../includes/validation_functions.php");?>

<?php 
    $admin = find_admin_by_id($_GET["id"]);
    if(!$admin){
        redirect_to("manage_admins.php");
    }
?>

<?php
    if(isset($_POST['submit'])){
        
        $required_fields = ["username", "password"];
        validate_presences($required_fields);

        $fields_with_max_length = ["username" => 30];
        validate_max_lengths($fields_with_max_length);
        
        if(empty($errors)){   
            $id = $admin["id"];
            $username = mysqli_prep($_POST["username"]);
            $password = mysqli_prep($_POST["password"]);

            $query = "UPDATE admins SET username = '{$username}', hashed_password = '{$password}' WHERE id = '{$id}' LIMIT 1";
            $result = mysqli_query($connection, $query);

            if($result && mysqli_affected_rows($connection) == 1){
                $_SESSION["message"] = "Admin Updated!";
                $_SESSION["created_subject_tracker"] = true;
                redirect_to("manage_admins.php");
            }else {
                $message = "Admin Update Failed!";
                $_SESSION["created_subject_tracker"] = false;
            }
        }
    }
?>

<?php $layout_context = "admin"; ?>
<?php include("../includes/layouts/header.php");?>
<div id="main">
    <div id="navigation">
    </div>
    <div id="page">
        <?php 
            if(!empty($message)){
                echo "<div class\"message\">" . htmlentities($message) . "</div>";
            }
        ?>
        <?php echo form_errors($errors);?>

        <h2>Edit Amin: <?php echo htmlentities($admin["username"]);?></h2>
        <form action="edit_admin.php?id=<?php echo urlencode($admin["id"]);?>" method="post">
            <p>Username:
                <input type="text" name="username" value="<?php echo htmlentities($admin["username"]);?>" />
            </p>
            <p>Password:
                <input type="password" name="password" value="" />
            </p>
            <input type="submit" name="submit" value="Save Changes" />
        </form>
        <br />
        <a href="manage_admins.php?id=<?php urlencode($admin["id"]);?>">Cancel</a>
    </div>
</div>
<?php include("../includes/layouts/footer.php");?>
<?php
require_once("../includes/session.php");
require_once("../includes/connection.php");
require_once("../includes/validation_functions.php");
require_once("../includes/functions.php");
include("../includes/layouts/header.php");

if(isset($_POST['submit'])){
    //process form
    $menu_name = mysqli_prep($_POST["menu_name"]);
    $position =(int) $_POST["position"];
    $visible =(int) $_POST["visible"];

    //validations
    $required_fields = ["menu_name","position","visible"];
    validate_presences($required_fields);
    $fields_with_max_lengths = ["menu_name" => 30];
    validate_max_lengths($fields_with_max_lengths);

    if(!empty($errors)){
        $_SESSION["errors"] = $errors;
        redirect_to("new_subject.php");
    }

    $query = "INSERT INTO `subjects`(`menu_name`, `position`, `visible`) VALUES ('$menu_name','$position','$visible')";
    $result = mysqli_query($connection, $query);

    if($result){
        $_SESSION["message"] = "Subject Created!";
        redirect_to("manage_content.php");
    }else {
        $_SESSION["message"] = "Subject Creation Failed!";
        redirect_to("new_subject.php");
    }
} else{
    redirect_to("new_subject.php");
}

mysqli_close($connection);
<?php
require_once("../includes/session.php");
require("../includes/connection.php");
require_once("../includes/functions.php");
require_once("../includes/validation_functions.php");?>

<?php
$username = "";
if(isset($_POST['submit'])){

    $required_fields = ["username", "password"];
    validate_presences($required_fields);

    $fields_with_max_length = ["username" => 30];
    validate_max_lengths($fields_with_max_length);

    if(empty($errors)){
        $username = $_POST["username"];
        $password = $_POST["password"];

        $found_admin = attempt_login($username, $password);

        if($found_admin){
            $_SESSION["admin_id"] = $found_admin["id"];
            $_SESSION["username"] = $found_admin["username"];
            redirect_to("admin.php");
        }else {
            $_SESSION["message"] = "Username/Password do not match!";
            $_SESSION["created_subject_tracker"] = false;
        }
    }
} else{

}
?>
<?php $layout_context = "admin"; ?>
<?php include("../includes/layouts/header.php");?>

    <div id="main">
        <div id="navigation">
        </div>

        <div id="page">
            <?php
            if(!empty($message)){
                echo "<div class\"message\">" . htmlentities($message) . "</div>";
            }
            ?>
            <h2>Login</h2>
            <form action="<?php $_SERVER['PHP_SELF']; ?>" method="post">
                <p>Username:
                    <input type="text" name="username" value="<?php echo htmlentities($username);?>" />
                </p>
                <p>Password:
                    <input type="password" name="password" value="" />
                </p>

                <input type="submit" name="submit" value="Login" />
            </form>
            <br />
            <a href="index.php">Cancel</a>
        </div>
    </div>
<?php include("../includes/layouts/footer.php");?>
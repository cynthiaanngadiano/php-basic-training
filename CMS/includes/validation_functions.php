<?php
$errors = [];

function fieldname_as_text($fieldname){
    $fieldname = str_replace("_"," ", $fieldname);
    $fieldname = ucfirst($fieldname);
    return $fieldname;
}

// PRESENCE
function presence($value) {
    return isset($value) && $value !== "";
}

function validate_presences($required_fields){
    global $errors;
    foreach ($required_fields as $field) {
        $value = trim($_POST[$field]);
        if (!presence($value)){
            $errors["$field"] = fieldname_as_text($field) . " cant be blank";
        }
    }
}

//STRING LENGTH
//maximum length
function maxLength($value, $max){
    return strlen($value) < $max;
}

function validate_max_lengths($fields_max_length) {
    global $errors;
    foreach ($fields_max_length as $field => $max) {
        $value = trim($_POST[$field]);
        if (!maxLength($value, $max)) {
            $errors[$field] = fieldname_as_text($field) . " is too long";
        }
    }
}

//INCLUSION IN A SET
function inclusion($value, $set) {
    return in_array($value, $set);
}
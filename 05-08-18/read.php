<?php
require('connection.php');
$view = "SELECT * FROM tbl_students";
$content = mysqli_query($connection,$view);
//test if query failed
if (!$content) {
    die("Query failed");
}
?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Database: Read</title>
    </head>
    <body>
    <table style="font-size: 15px" class="table table-striped table-bordered table-hover table-condensed small table-responsive">
        <tr style="font-size:20px">
            <th>ID</th>
            <th>Last Name</th>
            <th>First Name</th>
            <th>Middle Name</th>
            <th>Birthday</th>
        </tr>
        <?php while($row = mysqli_fetch_assoc($content)) { //mysqli_fetch_row or mysqli_fetch_array
            echo "<tr>";
            echo "<td>" . $row["stud_id"] . "</td>";
            echo "<td>" . $row["last_name"] . "</td>";
            echo "<td>" . $row["first_name"] . "</td>";
            echo "<td>" . $row["middle_name"] . "</td>";
            echo "<td>" . $row["birthday"] . "</td>";
            echo "</tr>";
        }
        ?>
    </table>
    <?php
    mysqli_free_result($content);
    ?>
    </body>
    </html>
<?php
mysqli_close($connection);
?>
/*
SQLyog Community v12.18 (32 bit)
MySQL - 5.7.21 : Database - exercise
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`exercise` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `exercise`;

/*Table structure for table `app` */

DROP TABLE IF EXISTS `app`;

CREATE TABLE `app` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `message` text NOT NULL,
  `posted` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `app` */

/*Table structure for table `tbl_students` */

DROP TABLE IF EXISTS `tbl_students`;

CREATE TABLE `tbl_students` (
  `stud_id` int(3) NOT NULL AUTO_INCREMENT,
  `first_name` char(10) NOT NULL,
  `middle_name` char(10) NOT NULL,
  `last_name` char(10) NOT NULL,
  `birthday` date NOT NULL,
  PRIMARY KEY (`stud_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_students` */

insert  into `tbl_students`(`stud_id`,`first_name`,`middle_name`,`last_name`,`birthday`) values 
(1,'Charlotte','Stars','Blits','2010-06-01'),
(2,'Zara','Stark','Cass','1993-02-23'),
(3,'Steff','Armin','Lloyd','2005-10-11'),
(5,'Hexa','Alex','Froy','1998-08-23');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
